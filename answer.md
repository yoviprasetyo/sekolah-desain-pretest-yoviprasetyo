1. Menggunakan PUT/PATCH sebaiknya pada kondisi ingin mengubah data yang sudah ada
2. Endpoint yang sama namun berbeda HTTP Method bisa mengakibatkan conflict
3. Model JSON untuk resource User
```json
{
    "links": {
        "self": "http://hostname/users?offset=1000&limit=1000",
        "next": "http://hostname/users?offset=2000&limit=1000",
        "prev": "http://hostname/users"
    },
    "data": [
        {
            "type": "user",
            "id": 1,
            "attributes": {
                "name": "Johny Walker",
                "email": "johny@walker.com"
            }
        }
    ]
}
```
4. Nomor 3 tapi dibuat menjadi Go
```go
type User {
    Id int `json:"id"`
    Name string `json:"name"`
    Email string `json:"email"`
}
var users []User
```
5. Lalu dibuat JSON
```json
{
    "user": {
        "id": 1,
        "name": "Johny Walker",
        "email": "johny@walker.com"
    }
}
```
6. Menggunakan empty interface ```go interface{}``` karena semua tipe variabel implement empty interface
7. Membuat array of string
8. Tidak ada bedanya
9. Terdapat di file [palindrom.go](palindrom/palindrom.go)
10. Kode Ketiga adalah kode Go yang aktif
11. Karena counter di dalam for menggunakan := yang artinya deklarasi variabel, sedangkan variabel yang dideklaraikan di dalam for, maka variabel tersebut hanya berlaku di scope for saja
12. Karena array slices pass by references
13. Terdapat di file [rest.go](rest/rest.go)