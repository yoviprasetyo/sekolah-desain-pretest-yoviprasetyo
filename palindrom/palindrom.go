package main

import (
	"fmt"
	"strings"
)

func Reserve(word string) string {
	finalString := ""
	length := len(word)
	for i := (length - 1); i >= 0; i-- {
		finalString += string(word[i])
	}
	return finalString
}

func main() {
	var word string
	fmt.Println("Masukkan teks")
	fmt.Scanln(&word)
	
	word = strings.ToLower(word)
	
	result := "bukanlah palindrom"
	if word == Reserve(word) {
		result = "adalah palindrom"
	}
	fmt.Println("Teks", word, result)
}