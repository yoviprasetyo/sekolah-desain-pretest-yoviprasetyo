package main

import (
	"encoding/json"
	"net/http"
)

type ToDo struct {
	Id int `json:"id"`
	Name string `json:"name"`
	Description string `json:"description"`
}

type ToDoResponse struct {
	Message string `json:"message"`
	Data []ToDo `json:"todos"`
}

var todos []ToDo

func todosRequest(writer http.ResponseWriter, r *http.Request) {
	writer.Header().Set("Content-Type", "application/json")

	if r.Method == http.MethodPost {
		
	}

	if r.Method == http.MethodGet {
		
	}

	if r.Method == http.MethodPut {
		
	}

	toDoResponse := ToDoResponse{"Todo list",todos}

	json.NewEncoder(writer).Encode(toDoResponse)
} 

func main() {
	http.HandleFunc("/todos", todosRequest)
	http.ListenAndServe(":3001", nil)
}